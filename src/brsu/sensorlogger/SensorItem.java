package brsu.sensorlogger;

public class SensorItem implements Cloneable {

	static String DELIM = " ";

	static class column {
		public static final String TIME = "timestamp";
		public static final String GPS = "location";
		public static final String STEPS = "stepdata";
		public static final String VELOCITY = "velocity";
		public static final String GRAVITY = "gravitydata";
		public static final String HEARTRATE = "heartrate";
		public static final String RR_INTERVAL = "rr_interval";
		public static final String GYRO = "gyrodata";
		public static final String MAGNETIC = "magneticdata";
		public static final String ACCELERATION = "accelerationdata";
		public static final String LINACCELERATION = "linearaccdata";
		public static final String ROTATION = "rotationdata";
		public static final String PRESSURE = "pressuredata";
		public static final String TREADMILL_VELOCITY = "treadmill_velocity";
		public static final String TREADMILL_SLOPE = "treadmill_slop";
		
	}

	public long timestamp;

	public double[] location = new double[3];
	public float location_accuracy;

	public float velocity;

	public float[] gyrodata = new float[3];
	int gyrodata_accuracy;

	public float[] gravitydata = new float[3];
	int gravity_accuracy;

	public float[] magneticdata = new float[3];

	public int magneticdata_accuracy;

	public float[] linearaccdata = new float[3];

	public int linearaccdata_accuracy;

	public float[] pressuredata = new float[3];

	public int pressuredata_accuracy;

	public float[] stepdata = new float[3];

	public int stepdata_accuracy;

	public float[] rotationdata = new float[3];

	public int rotationdata_accuracy;

	public float[] accelerationdata = new float[3];

	public int accelerationdata_accuracy;

	public int heartrate;

	public float treadmill_velocity;
	
	public float treadmill_slope;
	
	public int[] rr_interval = new int[4];
	
	public SensorItem clone() {
		SensorItem clone;
		try {
			clone = (SensorItem) super.clone();

			return clone;
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}

		return null;
	}
}
