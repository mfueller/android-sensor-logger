package brsu.sensorlogger;

import java.lang.reflect.Field;
import java.util.Vector;

import android.util.Log;

public class SensorItemPrinter {

	Vector<String> columns = new Vector<String>();
	Vector<Field> column_fields = new Vector<Field>();

	public String DELIM = ",";

	public SensorItemPrinter() {
	}

	public void setColumns(Vector<String> column_names) {
		this.columns = column_names;
		if (this.columns.contains(SensorItem.column.TIME) == false) {
			this.columns.add(0, SensorItem.column.TIME);
		}
		column_fields.clear();
	}

	public String printHeadline() {
		StringBuffer buffer = new StringBuffer();

		column_fields.clear();
		Class<SensorItem> cl = SensorItem.class;
		for (String c : this.columns) {
			try {
				Field field = cl.getField(c);

				if (field != null) {
					System.out.println("Name: " + c + " is " + field.toGenericString());
					column_fields.add(field);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		SensorItem ex = new SensorItem();

		for (Field f : column_fields) {
			try {
				Class type = f.getType();

				if (type == int.class) {
					buffer.append(f.getName());
					buffer.append(DELIM);
				} else if (type == double.class) {
					buffer.append(f.getName());
					buffer.append(DELIM);
				} else if (type == float.class) {
					buffer.append(f.getName());
					buffer.append(DELIM);
				} else if (type == boolean.class) {
					buffer.append(f.getName());
					buffer.append(DELIM);
				} else if (type == long.class) {
					buffer.append(f.getName());
					buffer.append(DELIM);
				} else if (type == double[].class) {
					double[] d = (double[]) f.get(ex);

					for (int i = 0; i < d.length; i++) {
						buffer.append(f.getName() + "[" + i + "]");
						buffer.append(DELIM);
					}
				} else if (type == float[].class) {
					float[] d = (float[]) f.get(ex);

					for (int i = 0; i < d.length; i++) {
						buffer.append(f.getName() + "[" + i + "]");
						buffer.append(DELIM);
					}
				} else if (type == int[].class) {
					int[] d = (int[]) f.get(ex);

					for (int i = 0; i < d.length; i++) {
						buffer.append(f.getName() + "[" + i + "]");
						buffer.append(DELIM);
					}
				} else {
					throw new Exception("Fieldformat not known");
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		Log.d("Printer", buffer.toString());
		return buffer.toString();
	}

	public String printItem(SensorItem item) {
		StringBuffer buffer = new StringBuffer();

		for (Field f : column_fields) {
			try {
				Class type = f.getType();
				Object value = f.get(item);
				if (type == int.class) {
					buffer.append(value);
					buffer.append(DELIM);
				} else if (type == double.class) {
					buffer.append(value);
					buffer.append(DELIM);
				} else if (type == float.class) {
					buffer.append(value);
					buffer.append(DELIM);
				} else if (type == boolean.class) {
					buffer.append(value);
					buffer.append(DELIM);
				} else if (type == long.class) {
					buffer.append(value);
					buffer.append(DELIM);
				} else if (type == double[].class) {
					double[] d = (double[]) value;

					for (int i = 0; i < d.length; i++) {
						buffer.append(d[i]);
						buffer.append(DELIM);
					}
				} else if (type == float[].class) {
					float[] d = (float[]) value;

					for (int i = 0; i < d.length; i++) {
						buffer.append(d[i]);
						buffer.append(DELIM);
					}
				} else if (type == int[].class) {
					int[] d = (int[]) value;

					for (int i = 0; i < d.length; i++) {
						buffer.append(d[i]);
						buffer.append(DELIM);
					}
				}else {
					throw new Exception("Fieldformat not known");
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		Log.d("Printer", buffer.toString());
		return buffer.toString();
	}
}
