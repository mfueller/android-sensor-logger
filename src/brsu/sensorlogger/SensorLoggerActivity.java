package brsu.sensorlogger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import android.app.Activity;
import android.app.Fragment;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import brsu.fueller.sensortest.R;

interface OnFragementInitalizedListener {
	public void onFragmentInitalized();
}

public class SensorLoggerActivity extends Activity implements SensorEventListener, LocationListener,
		OnFragementInitalizedListener {

	public final static String EVENT_START = "brsu.sensorlogger.event.START";
	public final static String EVENT_STOP = "brsu.sensorlogger.event.STOP";

	private Sensor gyrosensor;
	private SensorManager sensormanger;

	Vector<SensorItem> items = new Vector<SensorItem>();
	SensorItem last_item = new SensorItem();
	private FileOutputStream outputStream;
	private Sensor gravitysensor;
	private Sensor magneticsensor;
	long last_log_time = 0;

	int VIEW_UPDATE_CYCLE_TIME_MS = 1000;
	int LOG_CYCLE_TIME_MS = 1000;
	int SENSOR_UPDATE_CYCLE_IN_US = 5 * 1000;

	private Sensor linearaccsensor;
	private Sensor pressuresensor;
	private Sensor stepsensor;
	private Sensor rotationsensor;
	private Sensor accelerationsensor;
	private SensorItem item;
	private LocationManager locationManager;

	boolean isLogging = false;
	boolean isInitalized = false;

	private Notification.Builder sensorLoggerNotification;
	private int notificationID = 1;
	private NotificationManager mNotifyManager;
	private String timestamp;
	private long start_recording_time;
	private String log_file_name;
	private long last_treadmill_message = 0;

	SensorItemPrinter printer = new SensorItemPrinter();

	Timer logTimer = new Timer();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
		}

		mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		item = new SensorItem();

		Intent startIntent = new Intent(EVENT_START);
		Intent stopIntent = new Intent(EVENT_STOP);
		Intent displayIntent = new Intent(this, SensorLoggerActivity.class);

		PendingIntent pendingIntentStart = PendingIntent.getBroadcast(this, 0, startIntent, 0);
		PendingIntent pendingIntentStop = PendingIntent.getBroadcast(this, 0, stopIntent, 0);
		PendingIntent pendingIntentShowActivity = PendingIntent.getActivity(this, 0, displayIntent,
				PendingIntent.FLAG_NO_CREATE);

		sensorLoggerNotification = new Notification.Builder(this).setContentTitle("Android Sensor Logger")
				.setContentText("idle").setSmallIcon(R.drawable.ic_launcher)
				.setContentIntent(pendingIntentShowActivity)
				.addAction(R.drawable.ic_action_record, "start logging", pendingIntentStart)
				.addAction(R.drawable.ic_action_stop, "stop logging", pendingIntentStop);

		mNotifyManager.notify(notificationID, sensorLoggerNotification.build());

		initListeners();

	}

	@Override
	public void onFragmentInitalized() {
		loadPreferences();
	}

	private static final String PREF_NAME = "SensorLoggerPreference";
	private static final String PREF_LOGTIME = "logtime";
	private static final String PREF_COLUMNS = "columns";

	private void loadPreferences() {
		SharedPreferences settings = getSharedPreferences(PREF_NAME, 0);

		LOG_CYCLE_TIME_MS = settings.getInt(PREF_LOGTIME, LOG_CYCLE_TIME_MS);

		SensorDataView view = (SensorDataView) findViewById(R.id.sensorDataView1);
		if (view != null) {
			Vector<String> selectedItems = view.getSelectedItems();
			String defValue = new String();
			for (String s : selectedItems) {
				defValue += s + ";";
			}
			String columns = settings.getString(PREF_COLUMNS, defValue);

			Vector<String> strings = new Vector<String>();
			strings.addAll(Arrays.asList(columns.split(";")));

			view.setSelectedItems(strings);

			Spinner freqSpinner = (Spinner) findViewById(R.id.frequencySpinner);

			if (freqSpinner != null) {
				double freq = 1.0 / (((double) LOG_CYCLE_TIME_MS / 1000.0));

				String value = new DecimalFormat("#.#").format(freq);

				int pos = ((ArrayAdapter<String>) freqSpinner.getAdapter()).getPosition(value);
				freqSpinner.setSelection(pos);
			}
		}
	}

	private void savePreferences() {

		SharedPreferences settings = getSharedPreferences(PREF_NAME, 0);
		Editor editor = settings.edit();
		SensorDataView view = (SensorDataView) findViewById(R.id.sensorDataView1);
		if (view != null) {
			Vector<String> selectedItems = view.getSelectedItems();
			String defValue = new String();
			for (String s : selectedItems) {
				defValue += s + ";";
			}

			editor.putString(PREF_COLUMNS, defValue);

		}
		editor.putInt(PREF_LOGTIME, LOG_CYCLE_TIME_MS);

		editor.commit();

	}

	private void initListeners() {
		if (isInitalized == false) {
			locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);

			sensormanger = (SensorManager) getSystemService(SENSOR_SERVICE);
			List<Sensor> list = sensormanger.getSensorList(Sensor.TYPE_ALL);
			for (Sensor s : list) {
				Log.i("Test", "Sensor list item" + s.getName() + s.getType() + s.toString());
			}

			gyrosensor = sensormanger.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
			gravitysensor = sensormanger.getDefaultSensor(Sensor.TYPE_GRAVITY);
			magneticsensor = sensormanger.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
			linearaccsensor = sensormanger.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
			pressuresensor = sensormanger.getDefaultSensor(Sensor.TYPE_PRESSURE);
			stepsensor = sensormanger.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
			rotationsensor = sensormanger.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
			accelerationsensor = sensormanger.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

			sensormanger.registerListener(this, gyrosensor, SENSOR_UPDATE_CYCLE_IN_US);
			sensormanger.registerListener(this, gravitysensor, SENSOR_UPDATE_CYCLE_IN_US);
			sensormanger.registerListener(this, magneticsensor, SENSOR_UPDATE_CYCLE_IN_US);
			sensormanger.registerListener(this, linearaccsensor, SENSOR_UPDATE_CYCLE_IN_US);
			sensormanger.registerListener(this, pressuresensor, SENSOR_UPDATE_CYCLE_IN_US * 1000);
			sensormanger.registerListener(this, stepsensor, SENSOR_UPDATE_CYCLE_IN_US);
			sensormanger.registerListener(this, rotationsensor, SENSOR_UPDATE_CYCLE_IN_US);
			sensormanger.registerListener(this, accelerationsensor, SENSOR_UPDATE_CYCLE_IN_US);

			IntentFilter hbmFilter = new IntentFilter();
			hbmFilter.addAction("brsu.ble.heartrate");
			hbmFilter.addAction("brsu.ble.rr");
			hbmFilter.addAction("brsu.hbm.treadmill.log");

			this.registerReceiver(mBleBroadcastReceiver, hbmFilter);

			IntentFilter startStopFilter = new IntentFilter();
			startStopFilter.addAction(EVENT_START);
			startStopFilter.addAction(EVENT_STOP);

			this.registerReceiver(mBleStartStopEventReceiver, startStopFilter);

			isInitalized = true;
		}
	}

	protected void startLogger() {
		if (!isLogging) {

			init_pressure_data = 0.0;

			SensorDataView sensorview = (SensorDataView) findViewById(R.id.sensorDataView1);
			if (sensorview != null) {
				sensorview.setEnabled(false);
				printer.setColumns(sensorview.getSelectedItems());
			}

			Spinner freqSpinner = (Spinner) findViewById(R.id.frequencySpinner);
			if (freqSpinner != null) {
				freqSpinner.setEnabled(false);
			}

			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd.HHmmss");
			timestamp = sdf.format(new Date(System.currentTimeMillis()));

			log_file_name = "sensor_" + timestamp + ".log";

			try {

				log_file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
						log_file_name);

				Log.e("Test", "" + log_file);
				outputStream = new FileOutputStream(log_file);

				outputStream.write(printer.printHeadline().getBytes());
				outputStream.write("\n".getBytes());

				start_recording_time = System.currentTimeMillis();
				last_log_time = start_recording_time;

				writeView(R.id.logfile_name, log_file_name);

				item = new SensorItem();

			} catch (Exception e) {
				e.printStackTrace();
			}

			isLogging = true;

			sensorLoggerNotification.setContentText("running...[" + timestamp + "]");

			mNotifyManager.notify(notificationID, sensorLoggerNotification.build());

			ProgressBar pbar = (ProgressBar) findViewById(R.id.progressBar);

			if (pbar != null) {
				pbar.setIndeterminate(true);
			}
			logTimer = new Timer();

			TimerTask logTask = new TimerTask() {

				@Override
				public void run() {
					logItem();
				}
			};

			Spinner logSpinner = (Spinner) findViewById(R.id.frequencySpinner);

			if (logSpinner != null) {
				Object obj = logSpinner.getSelectedItem();

				if (obj != null) {
					String value = obj.toString();

					float fvalue = Float.parseFloat(value);
					fvalue = 1 / fvalue; // convert to time
					fvalue *= 1000; // to ms

					LOG_CYCLE_TIME_MS = (int) fvalue;
				}

			}

			savePreferences();
			logTimer.schedule(logTask, 0, this.LOG_CYCLE_TIME_MS);
		}
	}

	protected void stopLogger() {
		this.isLogging = false;

		logTimer.cancel();
		logTimer.purge();

		try {
			if (outputStream != null) {
				outputStream.close();

				// fix for problem with MTP cache refresh
				// https://code.google.com/p/android/issues/detail?id=38282
				MediaScannerConnection.scanFile(this, new String[] { log_file.getAbsolutePath() }, null, null);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		sensorLoggerNotification.setContentText("idle...");
		sensorLoggerNotification.setWhen(System.currentTimeMillis());
		sensorLoggerNotification.setProgress(0, 0, false);

		mNotifyManager.notify(notificationID, sensorLoggerNotification.build());

		ProgressBar pbar = (ProgressBar) findViewById(R.id.progressBar);

		if (pbar != null) {
			pbar.setIndeterminate(false);
		}

		start_recording_time = last_log_time;

		writeView(R.id.logfile_name, "");

		item = new SensorItem();

		SensorDataView sensorview = (SensorDataView) findViewById(R.id.sensorDataView1);
		if (sensorview != null) {
			sensorview.setEnabled(true);
		}

		Spinner freqSpinner = (Spinner) findViewById(R.id.frequencySpinner);
		if (freqSpinner != null) {
			freqSpinner.setEnabled(true);
		}
	}

	private final BroadcastReceiver mBleBroadcastReceiver = new BroadcastReceiver() {


		@Override
		public void onReceive(Context context, Intent intent) {
			final String action = intent.getAction();
			if ("brsu.ble.heartrate".equals(action)) {
				item.heartrate = intent.getIntExtra("brsu.ble.heartrate", -2);
				Log.i("SLog", "received heart beat data: " + item.heartrate);
			} else if ("brsu.ble.rr".equals(action)) {
				int[] tmp = intent.getIntArrayExtra("brsu.ble.rr");
				item.rr_interval = new int[]{0,0,0,0};
				for (int i=0; i<tmp.length;i++) {
					item.rr_interval[i] = tmp[i];
				}
				
				
				Log.i("SLog", "received rr data: " + formatArray(item.rr_interval,4));
			} else if("brsu.hbm.treadmill.log".equals(action)) {
				Log.i("LOG", "Received information from treadmill logger");
				last_treadmill_message = System.currentTimeMillis();
				item.treadmill_velocity = intent.getFloatExtra("velocity", 0.0f);
				item.treadmill_slope = intent.getFloatExtra("slope", 0.0f);
			}

		}
	};

	@Override
	public void finish() {
		Log.e("Test", "DONE");

		stopLogger();

		unregisterListeners();

		mNotifyManager.cancel(notificationID);

		savePreferences();

		super.finish();

	}

	private void unregisterListeners() {

		locationManager.removeUpdates(this);

		sensormanger = (SensorManager) getSystemService(SENSOR_SERVICE);
		List<Sensor> list = sensormanger.getSensorList(Sensor.TYPE_ALL);
		for (Sensor s : list) {
			Log.i("Test", "Sensor list item" + s.getName() + s.getType() + s.toString());
		}

		sensormanger.unregisterListener(this);

		this.unregisterReceiver(mBleBroadcastReceiver);
		this.unregisterReceiver(mBleStartStopEventReceiver);

	}

	private final BroadcastReceiver mBleStartStopEventReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			final String action = intent.getAction();
			if (EVENT_START.equals(action)) {
				SensorLoggerActivity.this.startLogger();
			} else if (EVENT_STOP.equals(action)) {
				SensorLoggerActivity.this.stopLogger();
			}

		}
	};

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// Log.i("Test", "onAccuracyChanged");
	}

	int counter = 0;
	private File log_file;
	private long last_step_timestamp;
	private long last_reset_time;
	private long last_view_update;
	private double init_pressure_data = 0.0;

	@Override
	public void onSensorChanged(SensorEvent event) {

		item.timestamp = event.timestamp;

		if (event.sensor == gyrosensor) {
			item.gyrodata = event.values;
			item.gyrodata_accuracy = event.accuracy;
		}

		if (event.sensor == gravitysensor) {
			item.gravitydata = event.values;
			item.gravity_accuracy = event.accuracy;
		}

		if (event.sensor == magneticsensor) {
			item.magneticdata = event.values;
			item.magneticdata_accuracy = event.accuracy;
		}

		if (event.sensor == linearaccsensor) {
			item.linearaccdata = event.values;
			item.linearaccdata_accuracy = event.accuracy;
		}

		if (event.sensor == pressuresensor) {
			item.pressuredata = event.values;
			item.pressuredata_accuracy = event.accuracy;

			if (init_pressure_data == 0.0) {
				init_pressure_data = item.pressuredata[0];
			}

			// Luftdruck auf meereshöhe //Barometeranzeige /
			// (1-Temperaturgradient*Höhe/Temperatur auf Meereshöhe in
			// Kelvin)^(0,03416/Temperaturgradient)

			item.pressuredata[1] = (float) (-7990.0 * Math.log(item.pressuredata[0] / init_pressure_data));
		}

		if (event.sensor == rotationsensor) {
			item.rotationdata = event.values;
			item.rotationdata_accuracy = event.accuracy;
		}

		if (event.sensor == stepsensor) {

			long delta_t = event.timestamp - last_step_timestamp;

			item.stepdata[0] = event.values[0];

			double dbl = (((double) delta_t) / (1000 * 1000 * 1000));
			double tmp_steps = 1.0 / dbl * 60.0;

			if (tmp_steps > 1000 || tmp_steps < 0) {
				// item.stepdata[1] = 0;
			} else {
				item.stepdata[1] = (float) tmp_steps;
			}

			item.stepdata[2] += item.stepdata[0];

			item.stepdata_accuracy = event.accuracy;

			last_step_timestamp = event.timestamp;

			last_reset_time = System.currentTimeMillis();
		}

		long ct = System.currentTimeMillis();
		long dt = ct - last_reset_time;
		if ((dt) > 3 * 1000) {
			item.stepdata[0] = 0;
			item.stepdata[1] = 0;
			last_reset_time = ct;
		}

		// Log.e("ns", "" + dt);

		if (event.sensor == accelerationsensor) {
			item.accelerationdata = event.values;
			item.accelerationdata_accuracy = event.accuracy;
		}

		if(last_treadmill_message + 10 * 1000 < System.currentTimeMillis()) {
			//treadmill values are outdated
			item.treadmill_slope = 0.0f;
			item.treadmill_velocity = 0.0f;
		}
		
		long currentTimeMillis = System.currentTimeMillis();
		long delta = (currentTimeMillis - last_view_update);
		if (delta > VIEW_UPDATE_CYCLE_TIME_MS) {
			Log.d("ASL", "receiving sensor data");
			updateView();
			last_view_update = System.currentTimeMillis();
		}

	}

	private void writeView(String id, String text) {
		SensorDataView sview = (SensorDataView) findViewById(R.id.sensorDataView1);

		if (sview != null) {
			TextView view = sview.getTextView(id);

			if (view != null) {
				view.setText(text);
			}
		}
	}

	private void writeView(String id, float value, int decimal_places) {
		SensorDataView sview = (SensorDataView) findViewById(R.id.sensorDataView1);

		if (sview != null) {
			TextView view = sview.getTextView(id);

			if (view != null) {
				view.setText(String.format("%2." + decimal_places + "f", value));
			}
		}
	}
	
	private void writeView(int id, String text) {
		TextView view = (TextView) findViewById(id);

		if (view != null) {
			view.setText(text);
		}
	}

	private String formatArray(float[] values, int length, int decimal_places) {
		return formatArray(values, length, decimal_places, "/");
	}
	private String formatArray(int[] values, int length) {
		String result = new String();
		String delimiter = ";";
		for (int i = 0; i < length; i++) {
			String tmp = ""+values[i];
			result += String.format("%4s", tmp);
			if (i < length - 1) {
				result += delimiter;
			}
		}
		return result;
	}
	private String formatArray(double[] values, int length, int decimal_places) {
		float[] d = new float[values.length];
		for (int i = 0; i < values.length; i++) {
			d[i] = (float) values[i];
		}

		return formatArray(d, length, decimal_places, "/");
	}

	private String formatArray(float[] values, int length, int decimal_places, String delimiter) {
		String result = new String();

		for (int i = 0; i < length; i++) {
			String tmp = String.format("%2." + decimal_places + "f", values[i]);
			result += String.format("%7s", tmp);
			if (i < length - 1) {
				result += delimiter;
			}
		}
		return result;
	}

	private void updateView() {

		if (isLogging) {
			SimpleDateFormat df = new SimpleDateFormat("mm:ss");
			String strTime = df.format(new Date(this.last_log_time - this.start_recording_time));
			this.writeView(R.id.loggingTime, strTime);
		} else {
			this.writeView(R.id.loggingTime, "--:--");
		}

		this.writeView(SensorItem.column.GPS, formatArray(item.location, 3, 4));
		this.writeView(SensorItem.column.HEARTRATE, "" + item.heartrate);
		this.writeView(SensorItem.column.RR_INTERVAL, formatArray(item.rr_interval,4));
		this.writeView(SensorItem.column.VELOCITY, item.velocity, 1);

		this.writeView(SensorItem.column.STEPS, formatArray(item.stepdata, 3, 1));

		this.writeView(SensorItem.column.GYRO, formatArray(item.gyrodata, 3, 3));
		this.writeView(SensorItem.column.GRAVITY, formatArray(item.gravitydata, 3, 3));

		this.writeView(SensorItem.column.MAGNETIC, formatArray(item.magneticdata, 3, 3));
		this.writeView(SensorItem.column.LINACCELERATION, formatArray(item.linearaccdata, 3, 3));
		this.writeView(SensorItem.column.ACCELERATION, formatArray(item.accelerationdata, 3, 3));
		this.writeView(SensorItem.column.ROTATION, formatArray(item.rotationdata, 3, 3));
		this.writeView(SensorItem.column.PRESSURE, formatArray(item.pressuredata, 2, 2));

		this.writeView(SensorItem.column.TREADMILL_VELOCITY, item.treadmill_velocity, 1);
		this.writeView(SensorItem.column.TREADMILL_SLOPE, item.treadmill_slope, 1);

	}

	@Override
	public void onLocationChanged(Location location) {

		Log.e("Test", "Got location");

		item.location[0] = location.getLongitude();
		item.location[1] = location.getLatitude();
		item.location[2] = location.getAltitude();
		item.location_accuracy = location.getAccuracy();

		item.velocity = location.getSpeed();

	}

	@Override
	public void onProviderDisabled(String provider) {

	}

	@Override
	public void onProviderEnabled(String provider) {

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	public SensorLoggerActivity() {
		Log.e("SLE", "New Activity");
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.e("Test", "PAUSE");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == R.id.close_item) {
			finish();
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	private void logItem() {
		if (isLogging) {
			try {
				outputStream.write(printer.printItem(item).getBytes());
				outputStream.write("\n".getBytes());

				outputStream.flush();
			} catch (Exception e) {
				e.printStackTrace();
			}
			counter += 1;

			if (counter % 10 == 0) {
				sensorLoggerNotification.setWhen(System.currentTimeMillis());
				sensorLoggerNotification.setProgress(0, 0, true);
				mNotifyManager.notify(notificationID, sensorLoggerNotification.build());

				counter = 1;
			}

			last_log_time = System.currentTimeMillis();
		}

		if (item.stepdata[0] > 0.1) {
			item.stepdata[0] = 0;
		}
	}

	static class PlaceholderFragment extends Fragment {

		OnFragementInitalizedListener listener;

		public PlaceholderFragment() {
		}

		@Override
		public void onAttach(Activity activity) {
			super.onAttach(activity);
			if (activity instanceof OnFragementInitalizedListener) {
				listener = (OnFragementInitalizedListener) activity;
			} else {
				throw new ClassCastException(activity.toString()
						+ " must implemenet MyListFragment.OnItemSelectedListener");
			}
		}

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);

			if (listener != null) {
				listener.onFragmentInitalized();
			}
		}

		@Override
		public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container, false);

			Button btnStart = (Button) rootView.findViewById(R.id.btnStart);

			if (btnStart != null) {
				btnStart.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent startIntent = new Intent(EVENT_START);
						inflater.getContext().sendBroadcast(startIntent);

					}
				});
			}

			Button btnStop = (Button) rootView.findViewById(R.id.btnStop);

			if (btnStop != null) {
				btnStop.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent stopIntent = new Intent(EVENT_STOP);
						inflater.getContext().sendBroadcast(stopIntent);

					}
				});
			}

			return rootView;
		}
	}

}
