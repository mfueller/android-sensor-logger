package brsu.sensorlogger;

import java.util.HashMap;
import java.util.Vector;

import android.content.Context;
import android.graphics.Path.FillType;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.webkit.WebSettings.TextSize;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class SensorDataView extends TableLayout {

	Vector<SensorItemViewElement> columns = new Vector<SensorItemViewElement>();
	HashMap<String, TextView> textView = new HashMap<String, TextView>();
	HashMap<String, CheckBox> checkBoxes = new HashMap<String, CheckBox>();

	public SensorDataView(Context context) {
		super(context);
		init();
	}

	// public SensorDataView(Context context, AttributeSet attrs, int
	// defStyleAttr) {
	// super(context, attrs, defStyleAttr);
	// init();
	// }

	public SensorDataView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	class SensorItemViewElement {

		public SensorItemViewElement(String id, String title, String unit) {
			super();
			this.id = id;
			this.title = title;
			this.unit = unit;
		}

		public String id;
		public String title;
		public String unit;
		public boolean active = true;
	}

	public TextView getTextView(String id) {
		return textView.get(id);
	}

	public void setEnabled(boolean enabled) {
		for (CheckBox b : checkBoxes.values()) {
			b.setEnabled(enabled);
		}
	}

	public Vector<String> getSelectedItems() {
		Vector<String> items = new Vector<String>();

		for (SensorItemViewElement e : columns) {
			if (e.active) {
				items.add(e.id);
			}
		}
		return items;
	}

	public void setSelectedItems(Vector<String> items) {
		for (SensorItemViewElement e : columns) {
			if (items.contains(e.id)) {
				e.active = true;
			} else {
				e.active = false;
			}
			CheckBox checkBox = checkBoxes.get(e.id);
			if (checkBox != null) {
				checkBox.setChecked(e.active);
			}
		}
	}

	public void init() {

		// SensorItem item = new SensorItem();

		columns.clear();
		textView.clear();
		checkBoxes.clear();

		columns.add(new SensorItemViewElement(SensorItem.column.GPS, "GPS:", "lat/lon/a"));
		columns.add(new SensorItemViewElement(SensorItem.column.STEPS, "Steps:", ""));
		columns.add(new SensorItemViewElement(SensorItem.column.VELOCITY, "Velocity:", "n/a"));
		columns.add(new SensorItemViewElement(SensorItem.column.GRAVITY, "Gravity", "x/y/z"));

		columns.add(new SensorItemViewElement(SensorItem.column.HEARTRATE, "HR:", "bpm"));
		columns.add(new SensorItemViewElement(SensorItem.column.RR_INTERVAL, "RR int.:", "ms"));
		columns.add(new SensorItemViewElement(SensorItem.column.GYRO, "Gyro:", "x/y/z"));
		columns.add(new SensorItemViewElement(SensorItem.column.MAGNETIC, "Magnetic:", "x/y/z"));
		columns.add(new SensorItemViewElement(SensorItem.column.ACCELERATION, "Acc:", "x/y/z"));
		columns.add(new SensorItemViewElement(SensorItem.column.LINACCELERATION, "Linacc:", "x/y/z"));
		columns.add(new SensorItemViewElement(SensorItem.column.ROTATION, "Rotation:", "x/y/z"));
		columns.add(new SensorItemViewElement(SensorItem.column.PRESSURE, "Pressure:", "hPa / dm"));

		columns.add(new SensorItemViewElement(SensorItem.column.TREADMILL_VELOCITY, "TrdMil vel:", "km/h"));
		columns.add(new SensorItemViewElement(SensorItem.column.TREADMILL_SLOPE, "TrdMil slp:", "%"));

		TableRow.LayoutParams params = new TableRow.LayoutParams();
		params.rightMargin = 10;
		params.weight = 1.0f;

		for (SensorItemViewElement e : columns) {
			final SensorItemViewElement fe = e;
			TableRow row = new TableRow(getContext());
			row.setLayoutParams(params);

			CheckBox cbActive = new CheckBox(getContext());
			cbActive.setChecked(e.active);
			cbActive.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					fe.active = isChecked;
				}
			});
			TextView title = new TextView(this.getContext());
			title.setTextSize(12);
			title.setText(e.title);

			TextView data = new TextView(this.getContext());
			data.setTypeface(Typeface.MONOSPACE);
			data.setText("");
			data.setLayoutParams(params);
			data.setGravity(Gravity.RIGHT);

			TextView unit = new TextView(this.getContext());
			unit.setTextSize(8);
			unit.setText(e.unit);

			row.addView(cbActive);
			row.addView(title);
			row.addView(data);
			row.addView(unit);

			this.addView(row);

			textView.put(e.id, data);
			checkBoxes.put(e.id, cbActive);
		}

	}
}
